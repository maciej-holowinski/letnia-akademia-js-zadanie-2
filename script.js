const map = document.querySelector(".googleMap");
const coordinates = document.querySelector(".coordinates");

navigator.geolocation.getCurrentPosition(success, error);

function success(position) {
    let latitude = position.coords.latitude,
        longitude = position.coords.longitude;
    map.src += `${latitude},${longitude}`;
    coordinates.textContent = `szerokość: ${latitude}, długość: ${longitude}`;
}

function error(error) {
    if (error.code == 1) {
        coordinates.textContent = `Brak pozwolenia na lokalizację`;
    }
}